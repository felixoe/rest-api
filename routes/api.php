<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{AuthController, KendaraanController, MobilController, MotorController};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api','prefix' => 'auth'], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);

    Route::get('kendaraan', [KendaraanController::class, 'index']);
    Route::post('kendaraan/store', [KendaraanController::class, 'store']);
    Route::get('kendaraan/show/{id}', [KendaraanController::class, 'show']);
    Route::post('kendaraan/update/{id}', [KendaraanController::class, 'update']);
    Route::post('kendaraan/destroy/{id}', [KendaraanController::class, 'destroy']);
    Route::get('kendaraan/report', [KendaraanController::class, 'allReport']);
    Route::get('kendaraan/report/{id}', [KendaraanController::class, 'report']);

    Route::get('mobil', [MobilController::class, 'index']);
    Route::post('mobil/store', [MobilController::class, 'store']);
    Route::get('mobil/show/{id}', [MobilController::class, 'show']);
    Route::post('mobil/update/{id}', [MobilController::class, 'update']);
    Route::post('mobil/destroy/{id}', [MobilController::class, 'destroy']);

    Route::get('motor', [MotorController::class, 'index']);
    Route::post('motor/store', [MotorController::class, 'store']);
    Route::get('motor/show/{id}', [MotorController::class, 'show']);
    Route::post('motor/update/{id}', [MotorController::class, 'update']);
    Route::post('motor/destroy/{id}', [MotorController::class, 'destroy']);
});
