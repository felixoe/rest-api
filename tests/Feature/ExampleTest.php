<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');
        $response = $this->get('api/auth/kendaraan');
        $response = $this->get('api/auth/kendaraan/report');
        $response = $this->get('api/auth/kendaraan/report/{1}');
        $response = $this->get('api/auth/kendaraan/show/{1}');
        $response = $this->get('api/auth/mobil');
        $response = $this->get('api/auth/mobil/show/{1}');
        $response = $this->get('api/auth/motor');
        $response = $this->get('api/auth/motor/show/{1}');

        $response->assertStatus(200);
    }
}
