## PANDUAN INSTALASI

1.  run composer install
2.  cp .env.example .env
3.  php artisan key:generate
4.  konfigurasi database
5.  php artisan migrate
6.  php artisan serve

## Fungsi yang harus ada

1.  Lihat Stok Kendaraan = route -> api/auth/kendaraan
2.  Penjualan Kendaraan = route -> api/auth/kendaraan/destroy/{id}
3.  Laporan Penjualan Per Kendaraan = route -> api/auth/kendaraan/report/{id}

## Pengerjaan harus menggunakan

1.  Service Repository Pattern = ada di folder app/repositories
2.  Strict class type = declare(strict_types=1); di setiap model
3.  Menyertakan unit test = ada di folder tests
4.  Format request dan response menggunakan standar HTTP REST API = ada di app/helpers/apiformat
5.  Menggunakan akses token untuk autentikasi (Json Web Token) = ada di file .env

## List API

| Domain | Method   | URI                             | Name | Action                                                     | Middleware                           |
+--------+----------+---------------------------------+------+------------------------------------------------------------+--------------------------------------+
|        | GET|HEAD | /                               |      | Closure                                                    | web                                  |
|        | GET|HEAD | api/auth/kendaraan              |      | App\Http\Controllers\KendaraanController@index             | api                                  |
|        | POST     | api/auth/kendaraan/destroy/{id} |      | App\Http\Controllers\KendaraanController@destroy           | api                                  |
|        | GET|HEAD | api/auth/kendaraan/report       |      | App\Http\Controllers\KendaraanController@allReport         | api                                  |
|        | GET|HEAD | api/auth/kendaraan/report/{id}  |      | App\Http\Controllers\KendaraanController@report            | api                                  |
|        | GET|HEAD | api/auth/kendaraan/show/{id}    |      | App\Http\Controllers\KendaraanController@show              | api                                  |
|        | POST     | api/auth/kendaraan/store        |      | App\Http\Controllers\KendaraanController@store             | api                                  |
|        | POST     | api/auth/kendaraan/update/{id}  |      | App\Http\Controllers\KendaraanController@update            | api                                  |
|        | POST     | api/auth/login                  |      | App\Http\Controllers\AuthController@login                  | api                                  |
|        | POST     | api/auth/logout                 |      | App\Http\Controllers\AuthController@logout                 | api                                  |
|        |          |                                 |      |                                                            | App\Http\Middleware\Authenticate:api |
|        | POST     | api/auth/me                     |      | App\Http\Controllers\AuthController@me                     | api                                  |
|        |          |                                 |      |                                                            | App\Http\Middleware\Authenticate:api |
|        | GET|HEAD | api/auth/mobil                  |      | App\Http\Controllers\MobilController@index                 | api                                  |
|        | POST     | api/auth/mobil/destroy/{id}     |      | App\Http\Controllers\MobilController@destroy               | api                                  |
|        | GET|HEAD | api/auth/mobil/show/{id}        |      | App\Http\Controllers\MobilController@show                  | api                                  |
|        | POST     | api/auth/mobil/store            |      | App\Http\Controllers\MobilController@store                 | api                                  |
|        | POST     | api/auth/mobil/update/{id}      |      | App\Http\Controllers\MobilController@update                | api                                  |
|        | GET|HEAD | api/auth/motor                  |      | App\Http\Controllers\MotorController@index                 | api                                  |
|        | POST     | api/auth/motor/destroy/{id}     |      | App\Http\Controllers\MotorController@destroy               | api                                  |
|        | GET|HEAD | api/auth/motor/show/{id}        |      | App\Http\Controllers\MotorController@show                  | api                                  |
|        | POST     | api/auth/motor/store            |      | App\Http\Controllers\MotorController@store                 | api                                  |
|        | POST     | api/auth/motor/update/{id}      |      | App\Http\Controllers\MotorController@update                | api                                  |
|        | POST     | api/auth/refresh                |      | App\Http\Controllers\AuthController@refresh                | api                                  |
|        |          |                                 |      |                                                            | App\Http\Middleware\Authenticate:api |
|        | POST     | api/auth/register               |      | App\Http\Controllers\AuthController@register               | api                                  |
|        | GET|HEAD | sanctum/csrf-cookie             |      | Laravel\Sanctum\Http\Controllers\CsrfCookieController@show | web                                  |
+--------+----------+---------------------------------+------+---------
