<?php
declare(strict_types=1);
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    use HasFactory;

    protected $table = 'report_penjualan';

    protected $fillable = [
        'kendaraan_id',
        'tahun_keluaran',
        'warna',
        'harga',
    ];
}
