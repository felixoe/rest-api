<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ApiFormat;
use App\Repositories\Motor\MotorRepository;

class MotorController extends Controller
{
    private $MotorRepository;

    public function __construct(MotorRepository $MotorRepository){
        $this->MotorRepository = $MotorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->MotorRepository->index();
        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = $this->MotorRepository->store();

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->MotorRepository->show($id);

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = $this->MotorRepository->update($id);

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->MotorRepository->destroy($id);

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }
}
