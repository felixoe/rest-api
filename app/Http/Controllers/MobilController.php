<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ApiFormat;
use App\Repositories\Mobil\MobilRepository;

class MobilController extends Controller
{
    private $MobilRepository;

    public function __construct(MobilRepository $MobilRepository){
        $this->MobilRepository = $MobilRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->MobilRepository->index();
        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $data = $this->MobilRepository->store();

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->MobilRepository->show($id);

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = $this->MobilRepository->update($id);

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->MobilRepository->destroy($id);

        if($data){
            return ApiFormat::createApi(200, 'success', $data);
        }else{
            return ApiFormat::createApi(400, 'failed');
        }
    }
}
