<?php

namespace App\Repositories\Mobil;

use Illuminate\Http\Request;

interface MobilRepository{
    public function index();
    public function store();
    public function show($id);
    public function update($id);
    public function destroy($id);
}
