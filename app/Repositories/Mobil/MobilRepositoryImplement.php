<?php

namespace App\Repositories\Mobil;

use Illuminate\Support\Facades\Validator;
use App\Models\Mobil;
use App\Models\Kendaraan;

class MobilRepositoryImplement implements MobilRepository{
    private $model;

    public function __construct(Mobil $model){
        $this->model = $model;
    }

    public function index()
    {
        return $this->model->all();
    }

    public function store()
    {
        $validator = Validator::make(request()->all(),[
            'kendaraan_id' => 'required',
            'mesin' => 'required',
            'kapasitas_penumpang' => 'required',
            'tipe' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $kendaraan = Kendaraan::first();

        $data = Mobil::create([
            'kendaraan_id' => $kendaraan->id,
            'mesin' => request('mesin'),
            'kapasitas_penumpang' => request('kapasitas_penumpang'),
            'tipe' => request('tipe'),
        ]);

        return $data->where('id','=',$data->id)->get();
    }

    public function update($id)
    {
        $validator = Validator::make(request()->all(),[
            'kendaraan_id' => 'required',
            'mesin' => 'required',
            'kapasitas_penumpang' => 'required',
            'tipe' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $data = Mobil::findOrFail($id);
        $kendaraan = Kendaraan::first();

        $data->update([
            'kendaraan_id' => $kendaraan->id,
            'mesin' => request('mesin'),
            'kapasitas_penumpang' => request('kapasitas_penumpang'),
            'tipe' => request('tipe'),
        ]);

        return $data->where('id','=',$data->id)->get();
    }

    public function show($id)
    {
        return $this->model->where('id','=',$id)->get();
    }

    public function destroy($id)
    {
        return $this->model->where('id','=',$id)->delete();
    }
}
