<?php

namespace App\Repositories\Kendaraan;

use Illuminate\Support\Facades\Validator;
use App\Models\Kendaraan;
use App\Models\Report;

class KendaraanRepositoryImplement implements KendaraanRepository{
    private $model;

    public function __construct(Kendaraan $model){
        $this->model = $model;
    }

    public function index()
    {
        return $this->model->all();
    }

    public function store()
    {
        $validator = Validator::make(request()->all(),[
            'tahun_keluaran' => 'required',
            'warna' => 'required',
            'harga' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $data = Kendaraan::create([
            'tahun_keluaran' => request('tahun_keluaran'),
            'warna' => request('warna'),
            'harga' => request('harga'),
        ]);

        return $data->where('id','=',$data->id)->get();
    }

    public function update($id)
    {
        $validator = Validator::make(request()->all(),[
            'tahun_keluaran' => 'required',
            'warna' => 'required',
            'harga' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $data = Kendaraan::findOrFail($id);

        $data->update([
            'tahun_keluaran' => request('tahun_keluaran'),
            'warna' => request('warna'),
            'harga' => request('harga'),
        ]);

        return $data->where('id','=',$data->id)->get();
    }

    public function show($id)
    {
        return $this->model->where('id','=',$id)->get();
    }

    public function destroy($id)
    {
        $data = Kendaraan::findOrFail($id);
        $report = Report::create([
            'kendaraan_id' => $id,
            'tahun_keluaran' => $data->tahun_keluaran,
            'warna' => $data->warna,
            'harga' => $data->harga,
        ]);
        $report->where('id','=',$report->id)->get();
        return $data->delete();
    }

    public function allReport()
    {
        return $data = Report::all();
    }

    public function report($id)
    {
        return $data = Report::where('id','=',$id)->get();
    }
}
