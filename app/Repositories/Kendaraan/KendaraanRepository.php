<?php

namespace App\Repositories\Kendaraan;

use Illuminate\Http\Request;

interface KendaraanRepository{
    public function index();
    public function store();
    public function show($id);
    public function update($id);
    public function destroy($id);
    public function allReport();
    public function report($id);
}
