<?php

namespace App\Repositories\Motor;

use Illuminate\Http\Request;

interface MotorRepository{
    public function index();
    public function store();
    public function show($id);
    public function update($id);
    public function destroy($id);
}
