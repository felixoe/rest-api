<?php

namespace App\Repositories\Motor;

use Illuminate\Support\Facades\Validator;
use App\Models\Motor;
use App\Models\Kendaraan;

class MotorRepositoryImplement implements MotorRepository{
    private $model;

    public function __construct(Motor $model){
        $this->model = $model;
    }

    public function index()
    {
        return $this->model->all();
    }

    public function store()
    {
        $validator = Validator::make(request()->all(),[
            'kendaraan_id' => 'required',
            'mesin' => 'required',
            'tipe_suspensi' => 'required',
            'tipe_transmisi' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $kendaraan = Kendaraan::first();

        $data = Motor::create([
            'kendaraan_id' => $kendaraan->id,
            'mesin' => request('mesin'),
            'tipe_suspensi' => request('tipe_suspensi'),
            'tipe_transmisi' => request('tipe_transmisi'),
        ]);

        return $data->where('id','=',$data->id)->get();
    }

    public function update($id)
    {
        $validator = Validator::make(request()->all(),[
            'kendaraan_id' => 'required',
            'mesin' => 'required',
            'tipe_suspensi' => 'required',
            'tipe_transmisi' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->messages());
        }

        $data = Motor::findOrFail($id);
        $kendaraan = Kendaraan::first();

        $data->update([
            'kendaraan_id' => $kendaraan->id,
            'mesin' => request('mesin'),
            'tipe_suspensi' => request('tipe_suspensi'),
            'tipe_transmisi' => request('tipe_transmisi'),
        ]);

        return $data->where('id','=',$data->id)->get();
    }

    public function show($id)
    {
        return $this->model->where('id','=',$id)->get();
    }

    public function destroy($id)
    {
        return $this->model->where('id','=',$id)->delete();
    }
}
