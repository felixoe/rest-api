<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Kendaraan\KendaraanRepository;
use App\Repositories\Kendaraan\KendaraanRepositoryImplement;
use App\Repositories\Mobil\MobilRepository;
use App\Repositories\Mobil\MobilRepositoryImplement;
use App\Repositories\Motor\MotorRepository;
use App\Repositories\Motor\MotorRepositoryImplement;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(KendaraanRepository::class, KendaraanRepositoryImplement::class);
        $this->app->bind(MobilRepository::class, MobilRepositoryImplement::class);
        $this->app->bind(MotorRepository::class, MotorRepositoryImplement::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
